<?php

/**
 *
 * Template Name: Home
 *
 */
get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php if (have_rows('cadastro_de_conteudo')) : ?>
    <?php while (have_rows('cadastro_de_conteudo')) : the_row(); ?>
        <?php if (get_row_layout() == 'home') : ?>
            <?php if (get_sub_field('home_background')) :
                $homeBackground = get_sub_field('home_background');
            endif ?>

            <section class="main home" style="background-image: url('<?php echo $homeBackground ?>');">
                <div class="d-flex flex-column justify-content-between h-100">
                    <div class="container">
                        <div class="col-12 align-content-center p-0">
                            <h1 class="col-6 p-0 col-lg-4">
                                <?php the_sub_field('home_titulo'); ?>
                            </h1>
                            <h2 class="col-6 p-0 home__h2">
                                <?php the_sub_field('home_subtitulo'); ?>
                            </h2>

                            <?php $home_botao = get_sub_field('home_botao'); ?>
                            <?php if ($home_botao) : ?>
                                <a href="<?php echo esc_url($home_botao['url']); ?>" target="<?php echo esc_attr($home_botao['target']); ?>" class="smooth__scroll">
                                    <div class="home__botao col-md-6 col-lg-3 mb-4">
                                        <?php echo esc_html($home_botao['title']); ?>
                                    </div>
                                </a>
                            <?php endif; ?>

                        </div>
                    </div>
                    <h3 class="">
                        Parcelas a partir de R$ 650
                    </h3>
                </div>
            </section>
        <?php elseif (get_row_layout() == 'ofertas') : ?>
            <section class="main oferta" id="oferta">
                <div class="container">

                    <h2 class="oferta__h2">
                        <?php the_sub_field('oferta_titulo'); ?>
                    </h2>
                    <p>
                        <?php the_sub_field('oferta_texto'); ?>
                    </p>
                    <?php $oferta_botao = get_sub_field('oferta_botao'); ?>
                    <?php if ($oferta_botao) : ?>
                        <a href="<?php echo esc_url($oferta_botao['url']); ?>" target="<?php echo esc_attr($oferta_botao['target']); ?>">

                            <div class="col-md-4 oferta__botao">
                                <?php echo esc_html($oferta_botao['title']); ?>
                            </div>
                        </a>
                    <?php endif; ?>
                </div>
            </section>
        <?php elseif (get_row_layout() == 'galeria_interna') : ?>
            <section class="main galeria-interna">

                <?php $galeria_urls = get_sub_field('galeria'); ?>
                <?php if ($galeria_urls) :  ?>
                    <div class="d-flex flex-wrap galeria__slick justify-content-center">
                        <?php foreach ($galeria_urls as $galeria_url) : ?>

                            <a href="<?php echo esc_url($galeria_url); ?>">
                                <img src="<?php echo esc_url($galeria_url); ?>" class="galeria-interna__imagens img-fluid" lazy='loading' alt="imagens internas do empreendimento" />
                            </a>

                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

            </section>
        <?php elseif (get_row_layout() == 'diferenciais') : ?>
            <section class="main diferenciais" id="diferenciais">
                <div class="container">
                    <h2 class="diferenciais__titulo">
                        <?php the_sub_field('diferenciais_titulo_principal'); ?>
                    </h2>
                    <p class="diferenciais__texto">
                        <?php the_sub_field('diferenciais_subtitulo'); ?>
                    </p>
                    <?php if (have_rows('diferenciais_itens')) : ?>
                        <div class="diferenciais-carousel">
                            <?php while (have_rows('diferenciais_itens')) : the_row(); ?>
                                <div class="d-flex flex-column mb-2 align-items-center justify-content-start">
                                    <?php if (get_sub_field('difenrenciais_icone')) : ?>
                                        <img src="<?php the_sub_field('difenrenciais_icone'); ?>" class="img-fluid my-auto col-6 col-md-4 col-lg-6" lazy='loading' alt="icones referentes aos diferenciais" />
                                    <?php endif ?>
                                    <p class="text-center diferenciais__icone__texto">
                                        <?php the_sub_field('diferenciais_titulo'); ?>
                                    </p>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>
                </div>
            </section>
        <?php elseif (get_row_layout() == 'galeria_externa') : ?>
            <section class="main galeria-externa">

                <?php $galeria_externa_urls = get_sub_field('galeria_externa'); ?>
                <?php if ($galeria_externa_urls) :  ?>
                    <div class="d-flex flex-wrap galeria-ex__slick justify-content-center">
                        <?php foreach ($galeria_externa_urls as $galeria_externa_url) : ?>
                            <a href="<?php echo esc_url($galeria_externa_url); ?>">
                                <img src="<?php echo esc_url($galeria_externa_url); ?>" class="galeria-externa__imagens img-fluid" lazy='loading' alt="imagens externas do empreendimento" />
                            </a>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

            </section>
        <?php elseif (get_row_layout() == 'detalhes') : ?>
            <section class="main detalhes">
                <div class="container">
                    <h2 class="detalhes__titulo">
                        <?php the_sub_field('detalhes_titulo_principal', false, false); ?>
                    </h2>
                    <div class="d-flex flex-wrap">
                        <?php if (have_rows('detalhes_itens')) : ?>
                            <?php while (have_rows('detalhes_itens')) : the_row(); ?>
                                <div class="d-flex mb-2 mb-md-3 col-12 col-md-6 p-0">
                                    <?php if (get_sub_field('check')) : ?>
                                        <img src="<?php the_sub_field('check'); ?>" class="img-fluid mb-auto mr-2 " lazy='loading' alt="check" />
                                    <?php endif ?>
                                    <p class="detalhes__texto">
                                        <?php the_sub_field('detalhes_texto'); ?>
                                    </p>
                                </div>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found 
                            ?>
                        <?php endif; ?>
                    </div>
                </div>
            </section>
        <?php elseif (get_row_layout() == 'video') : ?>
            <section class="main video" id="video">
                <div class="container">
                    <h2 class="video__titulo">
                        <?php the_sub_field('video_titulo', false, false); ?>
                    </h2>
                    <p class="mb-4">
                        <?php the_sub_field('video_texto'); ?>
                    </p>
                </div>
                <div class="embed-responsive embed-responsive-16by9">
                    <?php the_sub_field('video'); ?>
                </div>
            </section>
        <?php elseif (get_row_layout() == 'planta') : ?>
            <section class="planta" id="planta">
                <div class="container">
                    <h2 class="planta__titulo">
                        <?php the_sub_field('planta_titulo_principal'); ?>
                    </h2>
                    <div class="planta-carousel">
                        <?php if (have_rows('planta_imagem')) : ?>
                            <?php while (have_rows('planta_imagem')) : the_row(); ?>
                                <div class="d-flex flex-column planta__card col-lg-11 p-0">
                                    <?php if (get_sub_field('planta_imagem')) : ?>
                                        <a href="<?php the_sub_field('planta_imagem'); ?>">
                                            <img src="<?php the_sub_field('planta_imagem'); ?>" class="img-fluid" lazy='loading' alt="imagens das plantas do empreedimento" />
                                        </a>
                                    <?php endif ?>
                                    <p class="planta__texto">
                                        <?php the_sub_field('planta_texto'); ?>
                                    </p>
                                </div>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found 
                            ?>
                        <?php endif; ?>

                    </div>

                </div>
            </section>

        <?php elseif (get_row_layout() == 'informacoes_do_apartamento') : ?>
            <section class="main informacoes-apto">
                <div class="container">
                    <h2 class="informacoes-apto__titulo">
                        <?php the_sub_field('apartamento_titulo'); ?>
                    </h2>
                    <p class="informacoes-apto__texto">
                        <?php the_sub_field('apartamento_subtitulo'); ?>
                    </p>
                    <?php 
                        $apItems = get_sub_field('apartamento_itens', $page_ID);
                        if (is_array($apItems)) {
                            $rowCount = count($apItems);
                        }
                    ?>
                    <?php $i = 1; ?>
                    <?php if (have_rows('apartamento_itens')) : ?>
                        <div class="d-flex flex-wrap justify-content-center">
                            <?php while (have_rows('apartamento_itens')) : the_row(); ?>
                                <p class="informacoes-apto__itens">
                                    <span><?php the_sub_field('item'); ?></span>
                                    <?php if($i < $rowCount): ?>
                                    <span class="mx-1">|</span>
                                    <?php endif; ?>
                                </p>
                                <?php $i++; ?>
                            <?php endwhile; ?>
                        </div>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>
                </div>
            </section>
        <?php elseif (get_row_layout() == 'formulario') : ?>
            <section class="main formulario" id="contato">
                <div class="container">
                    <h2 class="formulario__titulo">
                        <?php the_sub_field('formulario_titulo', false, false); ?>
                    </h2>
                    <p class="formulario__texto">
                        <?php the_sub_field('formulario_subtitulo'); ?>
                    </p>
                    <div class="formulario__form">
                        <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
                    </div>
                </div>
            </section>
        <?php endif; ?>
    <?php endwhile; ?>
<?php else : ?>
    <?php
    ?>
<?php endif; ?>
<?php get_footer(); ?>