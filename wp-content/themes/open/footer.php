<!-- Footer -->
<footer class="main footer">
    <div class="container">
        <div class="d-flex align-items-center justify-content-between">
            <div class="d-flex justify-content-center mb-2 justify-content-md-start col-12 col-md-6">
                <?php if (get_field('logo_1', 'option')) : ?>
                    <img src="<?php the_field('logo_1', 'option'); ?>" class="img-fluid navbar__logos__andorinha" />
                <?php endif ?>
                <?php if (get_field('logo_2', 'option')) : ?>
                    <img src="<?php the_field('logo_2', 'option'); ?>" class="img-fluid my-auto ml-2" />
                <?php endif ?>
            </div>
            <ul class="d-none d-md-flex col-6">
                <li><a href="">Empreendimento</a></li>
                <li><a href="#diferenciais">Diferenciais</a></li>
                <li><a href="#video">Vídeo</a></li>
                <li><a href="#planta">Ficha Técnica</a></li>
                <li><a href="#contato">Contato</a></li>
            </ul>
        </div>

        <div class="d-flex justify-content-center justify-content-md-end">
            <div class="d-flex flex-column col-7 col-md-6">
                <span class="mb-2">
                    Contato:
                </span>
                <div class="d-flex footer__contato">
                    <a href="tel:<?php the_field('telefone_ddd', 'option'); ?><?php the_field('telefone', 'option'); ?>" class="d-flex">
                        <p class="footer__contato--size">
                            (<?php the_field('telefone_ddd', 'option'); ?>)
                        </p>
                        <p class="footer__contato__phone">
                            <?php the_field('telefone', 'option'); ?>
                        </p>
                    </a>
                </div>
                <div class="d-flex footer__contato">
                    <a href="tel:<?php the_field('whatsapp_ddd', 'option'); ?><?php the_field('whatsapp', 'option'); ?>" class="d-flex">
                        <p class="footer__contato--size">
                            (<?php the_field('whatsapp_ddd', 'option'); ?>)
                        </p>
                        <p class="footer__contato__phone">
                            <?php the_field('whatsapp', 'option'); ?>
                            <?php if (get_field('whatsapp_logo', 'option')) : ?>
                                <img src="<?php the_field('whatsapp_logo', 'option'); ?>" />
                            <?php endif ?>
                        </p>
                    </a>
                </div>
            </div>

            <div class="d-flex flex-column col-5 col-md-6">
                <span class="mb-2">
                    Redes Sociais:
                </span>
                <div>
                    <?php if (have_rows('cadastro_de_redes_sociais', 'option')) : ?>
                        <?php while (have_rows('cadastro_de_redes_sociais', 'option')) : the_row(); ?>
                            <a href="<?php the_sub_field('link'); ?>" target="_blank" class="mr-2">
                            <?php if (get_sub_field('icone')) : ?>
                                <img src="<?php the_sub_field('icone'); ?>" />
                            <?php endif ?>
                            </a>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <p class="footer__texto">
            <?php the_field('texto_', 'option', false, false); ?>
        </p>

    </div>


</footer>


<?php wp_footer(); ?>

</body>

</html>