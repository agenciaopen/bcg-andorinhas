<?php

function open_enqueue()
{
    wp_enqueue_style('slick-h2', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css', array(), rand(111, 9999), 'all');
    wp_enqueue_style('slickt-h2', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css', array(), rand(111, 9999), 'all');
    wp_enqueue_style( 'slick-lightbox-css', get_template_directory_uri() . '/assets/css/slick-lightbox.css', array(), rand(111,9999), 'all'  );

    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, false, true);
    wp_enqueue_script( 'slick-lightbox', get_template_directory_uri() . '/assets/js/slick-lightbox.js', array('jquery'),  '', 'all' );
    wp_enqueue_style('global-project', get_template_directory_uri() . '/assets/css/style.min.css', array(), rand(111, 9999), 'all');
    wp_enqueue_script('bs4-project', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'),  '', 'all');
    wp_enqueue_script('slick-h2-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js', array('jquery'), rand(111, 9999), 'all');
    wp_enqueue_script('global-project-js', get_template_directory_uri() . '/assets/js/global.js', array('jquery'),  rand(111, 9999), 'all');
    
    
/* quando da esse erro de not a function é o seguinte: 
repara que vc chamou a função do lightbox na linha 13 e chamou a lib na linha 14 
ou seja, ele chega na linha 13 e lê a function do lightbox, mas ele ainda não foi chamado no projeto.
então é só chamar a linha 14 antes da 13. Sacou? 

Outra coisa é la na home tem que passar o a href= com as imagens. Blz?

*/
}
add_action('wp_enqueue_scripts', 'open_enqueue',);
