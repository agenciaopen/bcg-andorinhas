
<section class="list_posts pt-lg-0" id=""> 
    <div class="container h-100">
        <div class="row justify-content-center align-items-stretch">


			<?php $loop = new WP_Query(array('order' => 'DESC', 'orderby' => 'DESC', 'post_type' => 'post', 'posts_per_page' => 1, 'paged' => get_query_var('paged') ? get_query_var('paged') : 1,)); ?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="row m-0 mt-5 align-items-center h-100 item w-100">
                    <div class="col-md-12 card">
                        <div class="card-body p-0">
                            <div class="row m-0 align-items-center">
                                <div class="col-md-8 pl-lg-5">
                                    <div class="col-md-12">
                                        <h3 class="mt-4"><a href="<?php echo get_permalink() ?>"><?php the_title()?></a></h3>
                                    </div>
                                    <div class="col-md-12 meta">
                                        <p><small>Por <?php the_author(); ?>, em <?php echo get_the_date(); ?></small></p>
                                        <hr>
                                    </div>
                                    <div class="col-md-10 description">
                                        <p>
                                            <a href="<?php echo get_permalink() ?>">
                                                <?php echo wp_trim_words( get_the_content(), 50, '...' );?>
                                            </a>
                                        </p>
                                    </div>
                                    <div class="card-footer p-0 pt-3">
                                        <div class="col-md-12">
                                            <a href="<?php echo get_permalink() ?>" class="text-uppercase">leia mais ></a>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4 p-0 text-center" >
                                    <a href="<?php echo get_permalink() ?>" class="img_link">    
                                        <span class="badge badge-primary d-none">Artigo</span>
                                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post), 'thumbnail' ); ?>
                                        <div class="col-md-12 p-0 bg_post" style="background-image: url('<?php echo $url; ?>');">

                                        </div>
                                    </a>
                                </div>
                                
                                
                            </div>
                        </div>
                    
                    </div>
                </div>
                <?php endwhile; ?><!--/.while -->
			<?php if ( is_search()) : ?>
				<div class="col-md-12 text-center main">
					<a href="/artigos">
						<div class="btn btn_call bounce-to-right">
							Ver todos
						</div>					
					</a>
				</div>
			<?php else : ?>
				<div class="col-md-12 col-sm-12 col-xs-12 text-center mt-5" id="pagination">
					<?php $big = 999999999; // need an unlikely integer
						echo paginate_links( array(
							'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'prev_text' => '<span><</span>',
							'next_text' => '<span>></span>',
							'total' => $loop->max_num_pages
						) );
					?>
				</div><!--/.pagination-->
			<?php endif; ?>
            </div>
            
        </div><!--/.container-->
    </div><!--/.row-->
</section><!--/.newsletter-->
